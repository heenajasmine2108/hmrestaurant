# -*- coding: utf-8 -*-
{
    'name': "hmrestaurant",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','product','report_xlsx','web','website'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'report/report.xml',
        'report/report_order_template_pdf.xml',
        'report/wizard_order_pdf.xml',
        'wizard/wizardorder_view.xml',
        'wizard/wizardorderxlsx_view.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/menu.xml',
        'views/attendance_view.xml',
        'views/categorymenu_view.xml',
        'views/customer_view.xml',
        'views/employee_view.xml',
        'views/menu_view.xml',
        'views/order_view.xml',
        'views/purchase_view.xml',
        'views/vendor_view.xml',
        'views/main_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'assets': {
        'web.assets_frontend': [
            'hmrestaurant/static/src/js/my_component.js',
        ],
    },
}
