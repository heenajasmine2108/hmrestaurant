/** @odoo-module **/
import { Component, tags, mount } from 'owl';

class MyComponent extends Component {
    static template = tags.xml/* xml */ `
        <div>
            <h1>Welcome to HM Restaurant</h1>
            <p>Powered by Owl</p>
        </div>
    `;
}

mount(MyComponent, document.getElementById('my_component'));
