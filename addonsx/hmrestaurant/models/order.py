from xml.dom import ValidationErr
from odoo import models, fields, api

class hmrestaurant(models.Model):
    _name = 'hmrestaurant.order'
    _description = 'Order'

    is_member = fields.Boolean(string='Is Member?')
    customer_id = fields.Many2one(comodel_name='hmrestaurant.customer', string='Customer Member Name')
    customer_name = fields.Char(string='Customer New Name')
    member_level = fields.Selection(related='customer_id.level', string='Member Level', readonly=True)
    transaction_date = fields.Date(string='Transaction Date', default=fields.Date.today())
    total_sales = fields.Integer(string='Total Sales', compute='_compute_total_sales', store=True)
    total_discount = fields.Integer(string='Total Discount', compute='_compute_total_sales', store=True)
    total_sales_before_discount = fields.Integer(string='Total Sales Before Discount', compute='_compute_total_sales', store=True)
    orderline_ids = fields.One2many(comodel_name='hmrestaurant.orderline', inverse_name='order_id', string='Order Lines')
    computed_customer_name = fields.Char(string='Customer Name', compute='_compute_customer_name', store=True)

    @api.depends('is_member', 'customer_id', 'customer_name')
    def _compute_customer_name(self):
        for order in self:
            if order.is_member:
                order.computed_customer_name = order.customer_id.name
            else:
                order.computed_customer_name = order.customer_name

    @api.depends('orderline_ids.subtotal')
    def _compute_total_sales(self):
        for order in self:
            order.total_sales = sum(order.orderline_ids.mapped('subtotal'))

    @api.depends('is_member', 'customer_id', 'customer_name', 'orderline_ids.subtotal')
    def _compute_total_sales(self):
        for order in self:
            total_sales = sum(order.orderline_ids.mapped('subtotal'))
            order.total_sales = total_sales
            order.total_sales_before_discount = total_sales

            if order.is_member:
                if order.member_level == 'bronze':
                    order.total_discount = total_sales * 0.05
                elif order.member_level == 'silver':
                    order.total_discount = total_sales * 0.10
                elif order.member_level == 'gold':
                    order.total_discount = total_sales * 0.15
                else:
                    order.total_discount = 0

                order.total_sales = total_sales - order.total_discount

class OrderLine(models.Model):
    _name = 'hmrestaurant.orderline'
    _description = 'Order Line'

    order_id = fields.Many2one(comodel_name='hmrestaurant.order', string='Order')
    menu_id = fields.Many2one(comodel_name='hmrestaurant.menu', string='Menu')
    quantity = fields.Integer(string='Quantity', default=1)
    subtotal = fields.Integer(string='Subtotal', compute='_compute_subtotal', store=True)

    @api.depends('menu_id.modal_price', 'quantity')
    def _compute_subtotal(self):
        for line in self:
            line.subtotal = line.menu_id.modal_price * line.quantity

    @api.model
    def create(self, vals):
        record = super(OrderLine, self).create(vals)
        if record.quantity and record.menu_id:
            menu = record.menu_id
            menu.stock -= record.quantity
        return record

    def write(self, vals):
        for line in self:
            if 'quantity' in vals:
                original_qty = line.quantity
                new_qty = vals.get('quantity', original_qty)
                line.menu_id.stock += (original_qty - new_qty)
        return super(OrderLine, self).write(vals)

    def unlink(self):
        for line in self:
            line.menu_id.stock += line.quantity
        return super(OrderLine, self).unlink()

    @api.constrains('quantity')
    def _check_quantity(self):
        for line in self:
            if line.quantity < 1:
                raise ValidationErr('Quantity for {} cannot be less than 1.'.format(line.menu_id.name))
            elif line.quantity > line.menu_id.stock:
                raise ValidationErr('Stock of {} is not sufficient. Maximum quantity is {}.'.format(line.menu_id.name, line.menu_id.stock))
