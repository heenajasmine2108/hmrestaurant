from odoo import models, fields, api

class CategoryMenu(models.Model):
    _name = 'hmrestaurant.categorymenu'
    _description = 'Category Menu'

    name = fields.Selection(string='Category Name', selection=[('new menu', 'New Menu'),
                                          ('promo', 'Promo'),
                                          ('combo rice', 'Combo Rice'),
                                          ('combo special', 'Combo Special'),
                                          ('ala carte', 'Ala Carte'),
                                          ('side dish and dessert', 'Side Dish & Dessert'),
                                          ('beverages', 'Beverages'),],
                                          required=True)
    no_shelf = fields.Integer(string='No shelf')
    # menu_ids = fields.One2many(comodel_name='hmresto.menu', 
    #                             inverse_name='categorymenu_id', 
    #                             string='Menu')