from odoo import models, fields, api

class Purchase(models.Model):
    _name = 'hmrestaurant.purchase'
    _description = 'Purchase'
    _rec_name = 'vendor_id'

    vendor_id = fields.Many2one(comodel_name='hmrestaurant.vendor', string='Vendor Name')
    trs_date = fields.Datetime(string='Transaction Date', default=fields.Datetime.now)
    purchasedetail_ids = fields.One2many(comodel_name='hmrestaurant.purchasedetail', inverse_name='purchase_id', string='Purchase Detail')
    total_purchase = fields.Integer(compute='_compute_total_purchase', string='Total Purchase', default=0)
    
    # Menghitung total pembelian berdasarkan subtotal di purchasedetail
    @api.depends('purchasedetail_ids')
    def _compute_total_purchase(self):
        for rec in self:
            a = self.env['hmrestaurant.purchasedetail'].search([('purchase_id', '=', rec.id)]).mapped('subtotal')
            rec.total_purchase = sum(a)

    # Mengurangi stok menu saat pembelian dihapus
    def unlink(self):
        for detail in self.purchasedetail_ids:
            detail.menu_id.stock -= detail.qty
        return super(Purchase, self).unlink()
    
    # Menyesuaikan stok menu saat pembelian diubah
    def write(self, vals):
        for rec in self:
            for detail in rec.purchasedetail_ids:
                detail.menu_id.stock -= detail.qty
        record = super(Purchase, self).write(vals)
        for rec in self:
            for detail in rec.purchasedetail_ids:
                detail.menu_id.stock += detail.qty
        return record

class PurchaseDetail(models.Model):
    _name = 'hmrestaurant.purchasedetail'
    _description = 'PurchaseDetail'
    _rec_name = 'menu_id'

    purchase_id = fields.Many2one(string='Purchase', comodel_name='hmrestaurant.purchase')
    menu_id = fields.Many2one(string='Purchase Items', comodel_name='hmrestaurant.menu')
    modal_price = fields.Integer(string='Modal Price')
    qty = fields.Integer(string='Quantity')
    subtotal = fields.Integer(compute='_compute_subtotal', string='Subtotal')

    # Mengisi harga modal berdasarkan menu yang dipilih
    @api.onchange('menu_id')
    def _onchange_menu(self):
        self.modal_price = self.menu_id.modal_price

    # Menghitung subtotal berdasarkan harga modal dan kuantitas
    @api.depends('modal_price', 'qty')
    def _compute_subtotal(self):
        for rec in self:
            rec.subtotal = rec.modal_price * rec.qty

    # Menambah stok menu saat purchasedetail dibuat
    @api.model
    def create(self, vals):
        record = super(PurchaseDetail, self).create(vals)
        if record.qty:
            self.env['hmrestaurant.menu'].search([('id', '=', record.menu_id.id)]).write({'stock': record.menu_id.stock + record.qty})
        return record

    # Menyesuaikan stok menu saat purchasedetail diubah
    def write(self, vals):
        for rec in self:
            original_qty = rec.qty
            new_qty = vals.get('qty', original_qty)
            if new_qty != original_qty:
                rec.menu_id.stock -= original_qty
                rec.menu_id.stock += new_qty
        return super(PurchaseDetail, self).write(vals)

    # Mengurangi stok menu saat purchasedetail dihapus
    def unlink(self):
        for rec in self:
            rec.menu_id.stock -= rec.qty
        return super(PurchaseDetail, self).unlink()
