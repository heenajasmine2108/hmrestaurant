from odoo import models, fields, api

class Vendor(models.Model):
    _name = 'hmrestaurant.vendor'
    _description = 'Vendor'

    name = fields.Char(string='Vendors name')
    pic = fields.Char(string='PIC')
    email = fields.Char(string='Email')
    phone = fields.Char(string='Phone')
    street1 = fields.Char(string='Street1')
    street2 = fields.Char(string='Street2')
    city = fields.Char(string='City')
    state = fields.Char(string='State')
    zip = fields.Integer(string='ZIP')
    country = fields.Char(string='Country')
    menu_ids = fields.Many2many(string='Menu', comodel_name='hmrestaurant.menu')
    