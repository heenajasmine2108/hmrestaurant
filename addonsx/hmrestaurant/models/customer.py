from odoo import models, fields, api

class Customer(models.Model):
    _name = 'hmrestaurant.customer'
    _description = 'Customer'

    name = fields.Char(string='Name')
    is_member = fields.Boolean(string='Member')
    level = fields.Selection([
        ('nonlevel', 'Nonlevel'),
        ('bronze', 'Bronze'),
        ('silver', 'Silver'),
        ('gold', 'Gold')
    ], string='level')
    phone = fields.Char(string='Phone')
    email = fields.Char(string='Email')
    street1 = fields.Char(string='Street1')
    street2 = fields.Char(string='Street2')
    city = fields.Char(string='City')
    state = fields.Char(string='State')
    zip = fields.Integer(string='ZIP')
    country = fields.Char(string='Country')
