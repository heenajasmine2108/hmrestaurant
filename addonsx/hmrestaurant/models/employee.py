from odoo import models, fields, api

class Employee(models.Model):
    _name = 'hmrestaurant.employee'
    _description = 'model.technical.name'

    name = fields.Char(string='Employees Name')
    phone = fields.Char(string='Work Phone')
    email = fields.Char(string='Work Email')
    job_position = fields.Selection(string='Job Position', selection=[('manager top', 'Manager TOP'),
                                          ('manager order', 'Manager order'),
                                          ('kasir', 'Kasir'),
                                          ('manager hr', 'Manager hr'),
                                          ('hr', 'HR'),
                                          ('manager purchasing', 'Manager purchasing'),
                                          ('purchasing assistant', 'Purchasing assistant'),                           
                                          ('lead chef', 'Lead chef'),
                                          ('chef', 'chef'),
                                          ('lead hostess', 'Lead hostess'),
                                          ('hostess', 'Hostess'),
                                          ('lead bartender', 'Lead bartender'),
                                          ('bartender', 'Bartender'),
                                          ('lead waitress', 'Lead waitress'),
                                          ('waitress', 'Waitress'),
                                          ('lead dishwasher', 'Lead dishwasher'),
                                          ('dishwasher', 'Dishwasher'),],
                                          required=True)
    

