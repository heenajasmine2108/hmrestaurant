from odoo import models, fields, api

class Attendance(models.Model):
    _name = 'hmrestaurant.attendance'
    _description = 'Attendance Record'

    employee_id = fields.Many2one(comodel_name='hmrestaurant.employee', string='Employee', required=True)
    check_in = fields.Datetime(string='Check In', required=True)
    check_out = fields.Datetime(string='Check Out')
    work_hours = fields.Float(string='Work Hours', compute='_compute_work_hours', store=True)

    @api.depends('check_in', 'check_out')
    def _compute_work_hours(self):
        for attendance in self:
            if attendance.check_in and attendance.check_out:
                delta = attendance.check_out - attendance.check_in
                attendance.work_hours = delta.total_seconds() / 3600.0
            else:
                attendance.work_hours = 0.0
