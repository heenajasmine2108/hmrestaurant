from odoo import models, fields, api

class Menu(models.Model):
    _name = 'hmrestaurant.menu'
    _description = 'Menu'

    name = fields.Char(string='Menu')
    categorymenu_id = fields.Many2one(string='Category', comodel_name='hmrestaurant.categorymenu')
    vendor_ids = fields.Many2many(comodel_name='hmrestaurant.vendor', string='Vendor')
    description = fields.Text(string='Description')
    stock = fields.Integer(string='Stock Quantity')
    stock_condition = fields.Char(compute='_compute_stock_condition', string='Stock Condition')
    modal_price = fields.Integer(string='Unit Price')
    total_modal = fields.Integer(compute='_compute_total_modal', string='Total Price')

    # Menghitung kondisi stok berdasarkan jumlah stok
    @api.depends('stock')
    def _compute_stock_condition(self):
        for rec in self:
            if rec.stock < 10:
                rec.stock_condition = 'less'
            else:
                rec.stock_condition = 'enough'
     
    # Menghitung total modal berdasarkan jumlah stok dan harga modal
    @api.depends('stock', 'modal_price')
    def _compute_total_modal(self):
        for record in self:
            record.total_modal = record.stock * record.modal_price
