from odoo import models, fields, api

class WizardOrderXlsx(models.TransientModel):
    _name = 'hmrestaurant.wizardorderxlsx'
    _description = 'Wizard Order XLSX'
    
    dari_tgl = fields.Date(string='From Date', required=True)
    ke_tgl = fields.Date(string='To Date', required=True)
    order_id = fields.Many2one(comodel_name='hmrestaurant.order', string='Order')
    
    def action_order_report(self):
        laporan = []
        dari = self.dari_tgl 
        ke = self.ke_tgl
        if dari:
            laporan += [('transaction_date', '>=', dari)]
        if ke:
            laporan += [('transaction_date', '<=', ke)]
        
        laporan_jadi = self.env['hmrestaurant.order'].search_read(laporan)
        
        data = {
            'form': self.read()[0],
            'laporannya': laporan_jadi,
        }
        
        report_action = self.env.ref('hmrestaurant.report_order_wizard_xlsx').report_action(self, data=data)
        report_action['close_on_report_download'] = True
        return report_action
    