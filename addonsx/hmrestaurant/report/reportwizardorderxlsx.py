from odoo import models, fields

class PenjualanWizardXlsx(models.AbstractModel):
    _name = 'report.hmrestaurant.wizard_order_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, laporannya):
        obj = data['laporannya']
        sheet = workbook.add_worksheet('Wizard Order')
        bold = workbook.add_format({'bold': True})
        
        # Write headers
        headers = ['Name', 'Level Member', 'Transaction Date', 'Total Sales Before Discount',
                   'Total Discount', 'Total Sales', 'Menu', 'Quantity', 'Subtotal']
        for col_num, header in enumerate(headers):
            sheet.write(0, col_num, header, bold)
        
        row = 1
        for x in obj:
            col = 0
            if x['is_member']:
                sheet.write(row, col, x['customer_id'][1] if x['customer_id'] else '')
                col += 1
                sheet.write(row, col, x['member_level'] if x['member_level'] else '')
            else:
                sheet.write(row, col, x['customer_name'] if x['customer_name'] else '')
                col += 1
                sheet.write(row, col, '')
            col += 1
            sheet.write(row, col, x['transaction_date'])
            col += 1
            sheet.write(row, col, x['total_sales_before_discount'])
            col += 1
            sheet.write(row, col, x['total_discount'])
            col += 1
            sheet.write(row, col, x['total_sales'])
            col += 1

            # Fetch details of each orderline_ids
            for detail_id in x['orderline_ids']:
                detail = self.env['hmrestaurant.orderline'].browse(detail_id)
                sheet.write(row, col, detail.menu_id.name if detail.menu_id else '')
                col += 1
                sheet.write(row, col, detail.quantity)
                col += 1
                sheet.write(row, col, detail.subtotal)
                col += 1
                row += 1  # Move to the next row for each detail
                col = 6  # Reset to the column after 'Total Harga' for new detail

            # Ensure to handle cases where there are no details
            if not x['orderline_ids']:
                row += 1  # Move to the next row for the next record

