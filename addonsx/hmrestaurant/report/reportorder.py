from odoo import models

class OrderXlsx(models.AbstractModel):
    _name = 'report.hmrestaurant.report_order_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, orders):
        sheet = workbook.add_worksheet('Order Report')

        # Header
        header_format = workbook.add_format({'bold': True, 'align': 'center', 'valign': 'vcenter'})
        sheet.set_row(0, 30, header_format)
        sheet.write(0, 0, 'Name', header_format)
        sheet.write(0, 1, 'Level Member', header_format)
        sheet.write(0, 2, 'Transaction Date', header_format)
        sheet.write(0, 3, 'Total Sales Before Discount', header_format)
        sheet.write(0, 4, 'Total Discount', header_format)
        sheet.write(0, 5, 'Total Sales', header_format)

        # Data
        row = 1
        for order in orders:
            col = 0
            if order.is_member:
                sheet.write(row, col, order.customer_id.name or '')
                sheet.write(row, col + 1, order.member_level or '')
            else:
                sheet.write(row, col, order.customer_name or '')
                sheet.write(row, col + 1, '')  # Leave member level blank for non-members
            sheet.write(row, col + 2, order.transaction_date.strftime('%Y-%m-%d') if order.transaction_date else '')
            sheet.write(row, col + 3, order.total_sales_before_discount)
            sheet.write(row, col + 4, order.total_discount)
            sheet.write(row, col + 5, order.total_sales)
            row += 1

        # Table 2: Order Lines
        sheet.write(row + 1, 0, 'Menu', header_format)
        sheet.write(row + 1, 1, 'Quantity', header_format)
        sheet.write(row + 1, 2, 'Subtotal', header_format)

        row += 2
        for order in orders:
            for line in order.orderline_ids:
                col = 0
                sheet.write(row, col, line.menu_id.name or '')
                sheet.write(row, col + 1, line.quantity)
                sheet.write(row, col + 2, line.subtotal)
                row += 1

        sheet.set_column(0, 0, 30)  # Adjust column width for the first column
