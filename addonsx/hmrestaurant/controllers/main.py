from odoo.http import request
from odoo import http

class RestaurantController(http.Controller):
    @http.route('/hmrestaurant', type='http', auth='public', website=True)
    def render_page(self):
        return http.request.render('hmrestaurant.my_owl_template')

    @http.route('/hmrestaurant/order', type='http', auth='public', website=True)
    def order_list(self):
        orders = request.env['hmrestaurant.order'].search([])
        return http.request.render('hmrestaurant.order_list_template', {'orders': orders})
    
    